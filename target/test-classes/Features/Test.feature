@Test
Feature: Landing Page Actions


  Scenario: Click View Courses
    Given I navigate to the landing page
    Then Assert I'm at the landing page
    Then I click on the  'View Courses' button
    Then I assert I'm at the Courses page

  Scenario: Click View All Courses
    Given I navigate to the landing page
    Then Assert I'm at the landing page
    Then I click on the 'View All Courses' button
    Then I assert I'm at the Courses page

  Scenario: Click Contact Us
    Given I navigate to the landing page
    Then Assert I'm at the landing page
    Then I click on the 'Contact us' button
    Then I assert I'm at the Contact page