package StepDefinitions;

import StepDefinitions.MainSteps;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.LandingPageObject;

import java.net.URISyntaxException;

public class LandingPageSteps {

    public MainSteps mainSteps;

    private WebDriver driver;

    private WebDriverWait wait;

    private LandingPageObject landingPageObject;

    public LandingPageSteps(MainSteps mainSteps) {
        this.mainSteps = mainSteps;
        driver = this.mainSteps.getDriver();
        wait = this.mainSteps.getWait();
        landingPageObject = PageFactory.initElements(driver, LandingPageObject.class);
    }

    @When("I click on the 'Contact us' button")
    public void iClickOnContactUSButton() throws InterruptedException {
        landingPageObject.clickOnFirstContactUSButton();
    }

    @When("I click on the  'View Courses' button")
    public void iClickOnViewCoursesButton() throws InterruptedException {
        landingPageObject.clickViewCoursesButton();
    }

    @When("I click on the 'View All Courses' button")
    public void iClickOnViewAllCoursesButton() throws InterruptedException {
        landingPageObject.clickOnViewAllCoursesButton();
    }

    @When("I click on the 'Create Account' button")
    public void iClickOnCreateAccount() throws InterruptedException {
        landingPageObject.clickOnCreateAccount();
    }

    @When("I assert I'm at the landing page")
    public void assertIamAtLandingPage() throws InterruptedException, URISyntaxException {
        landingPageObject.assertURL();
        //landingPageObject.assertMainHeaderIsPresent();
       // landingPageObject.assertViewCoursesIsPresent();
       // landingPageObject.assertViewAllCoursesIsPresent();
        //landingPageObject.assertCreateAccountIsPresent();

    }

}
