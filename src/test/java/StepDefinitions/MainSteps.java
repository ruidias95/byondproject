package StepDefinitions;

import driverManagers.driverManager;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.LandingPageObject;

import java.net.MalformedURLException;
import java.time.Duration;

public class MainSteps {

    public WebDriverWait initialWait;

    public WebDriver driver;
    private LandingPageObject landingPageObject;

    private final driverManagers.driverManager webDriverManager;
    private LandingPageSteps landingPageSteps;


    public MainSteps() {
        webDriverManager = new driverManagers.driverManager();
        driver = webDriverManager.getDriver();
        //initialWait = new WebDriverWait(driver, Duration.ofSeconds(60));
        //landingPageObject = PageFactory.initElements(driver, LandingPageObject.class);
    }


    @Given("I navigate to the landing page")
    public void iNavigateTo() throws InterruptedException, MalformedURLException {
        driver.navigate().to("https://ultimateqa.com/fake-landing-page/");
        String activeWindow = driver.getWindowHandle();
        driver.switchTo().window(activeWindow);
        Thread.sleep(4000);
    }


    public WebDriver getDriver() {
        return driver;
    }

    public WebDriverWait getWait() {return initialWait;}

}
