package StepDefinitions;

import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.CoursesPageObject;

import java.net.URISyntaxException;

public class CoursesSteps {


    public MainSteps mainSteps;

    private WebDriver driver;

    private WebDriverWait wait;

    private CoursesPageObject coursesPageObject;

    public CoursesSteps(MainSteps mainSteps) {
        this.mainSteps = mainSteps;
        driver = this.mainSteps.getDriver();
        wait = this.mainSteps.getWait();
        coursesPageObject = PageFactory.initElements(driver, CoursesPageObject.class);
    }
    @Then("I assert I'm at the Courses page")
    public void assertImAtTheCoursesPage() throws URISyntaxException {
        coursesPageObject.assertURL();
    }
}
