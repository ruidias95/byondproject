package StepDefinitions;

import StepDefinitions.MainSteps;
import io.cucumber.java.en.Then;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.ContactPageObject;

import java.net.URISyntaxException;

public class ContactUsSteps {

    public MainSteps mainSteps;

    private WebDriver driver;

    private WebDriverWait wait;

    private ContactPageObject contactUs;



    public ContactUsSteps(MainSteps mainSteps) {
        this.mainSteps = mainSteps;
        driver = this.mainSteps.getDriver();
        wait = this.mainSteps.getWait();
        contactUs = PageFactory.initElements(driver, ContactPageObject.class);
    }

    @Then("I should see the contact us header")
    public void iShouldSeeTheContactUsHeader() throws InterruptedException {
        contactUs.assertHeaderContactUS();
    }

    @Then("I assert I'm at the Contact page")
    public void iAssertImAtTheContactPage() throws URISyntaxException, InterruptedException {
        contactUs.assertURL();
        contactUs.assertHeaderContactUS();
    }



}
