package driverManagers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import java.time.Duration;

public class driverManager {

    private  WebDriver driver;

    public driverManager() {
    }

    public WebDriver getDriver() {
        if (driver == null)
            driver = createDriver();
        return driver;
    }

    public  WebDriver createDriver() {

        driver = createLocalDriver();

        return driver;
    }

    public enum DriverType {
        FIREFOX, CHROME
    }

    public enum OS {
        WINDOWS, MAC
    }

    public static OS getOperatingSystem() {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            return OS.WINDOWS;
        } else if (os.contains("mac")) {
            return OS.MAC;
        }

        return null;
    }

    private WebDriver createLocalDriver() {
        switch (DriverType.FIREFOX) {
            case FIREFOX:
                driver = io.github.bonigarcia.wdm.WebDriverManager.firefoxdriver().create();
                break;

            case CHROME:
                System.out.println("Chrome Operating System");
               // options.addArguments("--disable-backgrounding-occluded-windows");
                //options.addArguments("force-device-scale-factor=0.75");
                switch (getOperatingSystem()) {
                    case WINDOWS:
                        System.out.println("Windows Operating System");
                        io.github.bonigarcia.wdm.WebDriverManager.chromedriver().setup();
                        driver = new ChromeDriver();
                        System.out.println(driver.getPageSource());
                        //driver = io.github.bonigarcia.wdm.WebDriverManager.chromedriver().capabilities(options).create();
                        break;

                    case MAC:
                        System.out.println("Mac Operating System");
                        //options.addArguments("start-fullscreen");
                        //driver = io.github.bonigarcia.wdm.WebDriverManager.chromedriver().capabilities(options).create();
                        break;
                }

        }

        return driver;
    }
}