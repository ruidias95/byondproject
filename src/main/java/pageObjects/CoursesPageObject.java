package pageObjects;

import Util.utils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;

public class CoursesPageObject {

    public CoursesPageObject(WebDriver driver) throws URISyntaxException {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
    public WebDriver driver;

    public URI coursesPageURL = new URI("https://courses.ultimateqa.com/collections");

    public void assertURL() throws URISyntaxException {
        URI actualCoursesPageURL = new URI(driver.getCurrentUrl());
        assert coursesPageURL.equals(actualCoursesPageURL);
    }
}
