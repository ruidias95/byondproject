package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;

public class ContactPageObject {

    @FindBy(how = How.XPATH, using = "//div[@class='et_pb_text_inner']/h1[contains(text(),'Contact Us')]")
    private WebElement contactUsHeader;

    private final String contatcUsURL = "https://ultimateqa.com/contact/";

    private WebDriver driver;
    private WebDriverWait wait;
    public URI contactPageURL = new URI("https://ultimateqa.com/contact/");


    public ContactPageObject(WebDriver driver) throws URISyntaxException {

        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(60));
        this.driver = driver;
    }

    public void assertHeaderContactUS() throws InterruptedException {
        Util.utils.scrollToElement(contactUsHeader);
        assert contactUsHeader.isDisplayed();
    }

    public void assertURL() throws URISyntaxException {
        URI actualContactUsURL = new URI(driver.getCurrentUrl());
        assert contactPageURL.equals(actualContactUsURL);
    }

}
