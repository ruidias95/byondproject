package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Util.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;

public class LandingPageObject {

    private static utils utils;
    public LandingPageObject(WebDriver driver) throws URISyntaxException {
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, Duration.ofSeconds(60));
        utils = PageFactory.initElements(driver, utils.class);
        this.driver = driver;

    }

    @FindBy(how = How.XPATH, using = "//a[contains(text(),'View Courses')]")
    private WebElement viewCoursesButton;

    @FindBy(how = How.XPATH, using = "(//a[contains(text(),'View All Courses')])[1]")
    private WebElement viewAllCoursesButton;

    @FindBy(how = How.XPATH, using = "(//a[@class='et_pb_button et_pb_button_3 et_hover_enabled et_pb_bg_layout_light'][contains(text(),'Contact Us')])[1]")
    public WebElement firstContactUsButton;

    @FindBy(how = How.XPATH, using = "//a[@class='et_pb_button et_pb_button_4 et_hover_enabled et_pb_bg_layout_light'][contains(text(),'Create Account')]")
    private WebElement createAccount;

    @FindBy(how = How.XPATH, using = "//div[@class='et_pb_text_inner']/h1[contains(text(),'Learn to Code Websites, Apps & Games')]")
    private WebElement mainHeader;


    private WebDriverWait wait;
    public WebDriver driver;

    public URI landingPageURL = new URI("https://ultimateqa.com/fake-landing-page/");


    public void clickOnFirstContactUSButton() throws InterruptedException {
        utils.scrollToElement(firstContactUsButton);
        firstContactUsButton.click();
    }

    public void clickViewCoursesButton() throws InterruptedException {
        utils.scrollToElement(viewCoursesButton);
        viewCoursesButton.click();
    }

    public void clickOnViewAllCoursesButton() throws InterruptedException {
        utils.scrollToElement(viewAllCoursesButton);
        viewAllCoursesButton.click();
    }

    public void clickOnCreateAccount() throws InterruptedException {
        utils.scrollToElement(createAccount);
        createAccount.click();
    }

    public void assertURL() throws URISyntaxException {
        URI actualLandingPageURL = new URI(driver.getCurrentUrl());
        assert landingPageURL.equals(actualLandingPageURL);
    }

    public void assertViewCoursesIsPresent() throws InterruptedException {
       wait.until(ExpectedConditions.
               presenceOfElementLocated(By.
                       xpath("//a[@class='et_pb_button et_pb_button_1 et_hover_enabled et_pb_bg_layout_light']" +
                               "[contains(text(),'View All Courses')]")));
    }

    public void assertViewAllCoursesIsPresent() throws InterruptedException {
        wait.until(ExpectedConditions.
                presenceOfElementLocated(By.
                        xpath("//a[@class='et_pb_button et_pb_button_2 et_hover_enabled et_pb_bg_layout_light']" +
                                "[contains(text(),'View All Courses')]")));
    }

    public void assertMainHeaderIsPresent() throws InterruptedException {
        wait.until(ExpectedConditions.
                presenceOfElementLocated(By.
                        xpath("//div[@class='et_pb_text_inner']/h1[contains(text(),'Learn to Code Websites, Apps & Games')]")));
    }

    public void assertCreateAccountIsPresent() {
        wait.until(ExpectedConditions.
                presenceOfElementLocated(By.
                        xpath("//a[@class='et_pb_button et_pb_button_4 et_hover_enabled et_pb_bg_layout_light']" +
                                "[contains(text(),'Create Account')]")));
    }
}
