package Util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class utils {

    public static WebDriver driver;


    public WebDriverWait initiateWait;


    public utils(WebDriver driver) {

        this.driver = driver;
        initiateWait = new WebDriverWait(driver, Duration.ofSeconds(60));
    }

    public static void scrollToElement(WebElement elementToMove) throws InterruptedException {

        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elementToMove);
        Thread.sleep(2000);

    }
}
